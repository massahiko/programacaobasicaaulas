﻿using System;
using System.Text;

namespace Aula1
{
    class Program
    {
        static void Main(string[] args)
        {
            //cadeia nome, sobrenome
            string nome; string sobrenome;

            //cadeia origem
            string origem;

            //inteiro idade
            int idade;

            // escreva("Informe seu nome e sobrenome: ")
            Console.WriteLine("Informe seu nome e sobrenome: ");

            //leia(nome, sobrenome)
            nome = Console.ReadLine();
            sobrenome = Console.ReadLine();

            //escreva("Olá,", nome, sobrenome, ", bem vindo(a)!")

            // StringBuilder - melhor forma de concatenar
            //var nomeCompletoBuilder = new StringBuilder();
            //nomeCompletoBuilder.Append("Olá ")
            //				   .Append(nome)
            //				   .Append(sobrenome)
            //				   .Append(", bem vindo(a)!");

            var nomeCompleto = $"Olá {nome}{sobrenome} , bem vindo(a)!";
            Console.WriteLine("Olá, {0} {1}, bem vindo(a)!", nome, sobrenome);

            //escreva("\nPara continuarmos preciso que você preencha os dados a seguir")
            Console.WriteLine("Para continuarmos preciso que você preencha os dados a seguir");

            //escreva("\nInforme sua origem: ")
            Console.WriteLine("Informe sua origem: ");

            //leia(origem)
            origem = Console.ReadLine();

            //escreva("Agora informe sua idade: ")
            Console.WriteLine("Agora informe sua idade: ");

            //leia(idade)
            idade = int.Parse(Console.ReadLine());

            //escreva("Por favor, confirme se os dados informados estão corretos:")
            Console.WriteLine("Por favor, confirme se os dados informados estão corretos: ");

            //escreva("Seu nome é "+nome+""+sobrenome+", você nasceu em "+origem+" e atualmente tem "+idade+"?")
            Console.WriteLine("Seu nome é {0} {1}, você nasceu em {2} e atualmente tem {3}?", nome, sobrenome, origem, idade);
        }
    }
}
